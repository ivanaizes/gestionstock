package com.stock.mvc.entite;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="commandeclient")

public class CommandeClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long idCommandeClient;
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommandeClient;	
	@ManyToOne
	@JoinColumn(name="idClient")	
	private Client client;		
	@OneToMany(mappedBy="commandeClient")	
	private List<LigneCommandeClient> ligneCommandeClient;


	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getDateCommandeClient() {
		return dateCommandeClient;
	}
	public void setDateCommandeClient(Date dateCommande) {
		this.dateCommandeClient = dateCommande;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}	
	
	
	public long getIdCommandeClient() {
		return idCommandeClient;
	}
	public void setIdCommandeClient(long idCommandeClient) {
		this.idCommandeClient = idCommandeClient;
	}	
	public List<LigneCommandeClient> getLigneCommandeClient() {
		return ligneCommandeClient;
	}
	
	public void setLigneCommandeClient(List<LigneCommandeClient> ligneCommandeClient) {
		this.ligneCommandeClient = ligneCommandeClient;
	}	
		
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
